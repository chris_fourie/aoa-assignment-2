/*
 * Copyright (C) 2019 chrisfourie.africa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Experiments;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import static Tools.utilities.ExperimentHelper.*;

/**
 *
 * @author chrisfourie.africa
 */
public class Run_Experiments {

    public static void main(String[] args) throws IOException {
        //Intro//
        System.out.println("Welcome to your experiment\n\n");

        //Hyperparameters
        int numOfArr = 12;
        int range = 1000000;   //What range should random elements be generated?
        int[] arrLengths = {10, 100, 500, 1000, 5000 , 10000, 50000 , 100000, 400000, 600000, 800000, 1000000}; //must match numOfArr
        int[] baseArr = makeRandArr(arrLengths[numOfArr-1], range);
        int numRuns = 100;
        boolean allRand = true; //false -> all runs use the same base array //true -> all runs use a random pseudo-randomised array
        int maxThreshold = 20000;
        int aveThreshold = 8000;

        //create file to save results
        LocalDateTime now = LocalDateTime.now();
        FileWriter csvWriter = new FileWriter("results/Results_"+now+"_" + allRand + "MT"+ maxThreshold + "AT"
                + aveThreshold +".csv");
        csvWriter.write("Type");
        csvWriter.write(",");
        csvWriter.write("ArraySize");
        csvWriter.write(",");
        csvWriter.write("Elapse_Time_Ave");
        csvWriter.write(",");
        csvWriter.write("Elapse_Time_Max");
        csvWriter.write(",");
        csvWriter.write("Corrected_Elapse_Time_Max");
        csvWriter.write("\n");

//        Exp1_1.run(baseArr,arrLengths,csvWriter,numOfArr,numRuns, allRand, maxThreshold, aveThreshold);
//        Exp1_2.run(baseArr,arrLengths,csvWriter,numOfArr,numRuns, allRand, maxThreshold, aveThreshold);
        Exp2_1.run(baseArr,arrLengths,csvWriter,numOfArr,numRuns, allRand, maxThreshold, aveThreshold);
        Exp2_2.run(baseArr,arrLengths,csvWriter,numOfArr,numRuns, allRand, maxThreshold, aveThreshold);





        //save results and close
        csvWriter.close();


    }


}



