/*
 * Copyright (C) 2019 chrisfourie.africa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Experiments;

import Tools.TreeComponents.Node;
import Tools.Algorithms.BST;
import Tools.Algorithms.OS;
import Tools.Algorithms.RB_BST;
import Tools.utilities.PrintHelper;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Random;

import static Tools.Algorithms.OS.buildOS;
import static Tools.Algorithms.RB_BST.buildRB_BST;
import static Tools.utilities.ExperimentHelper.*;


/**
 *
 * @author chrisfourie.africa
 */
public class Exp1_1 {

    //EXPERIMENT 1_1 - RB-Insert

    public static void run (int[] baseArr, int[] arrLengths, FileWriter csvWriter, int numOfArr, int numRuns, boolean allRand, int maxThreshold, int aveThreshold) throws IOException {
        System.out.println("\n ************** \n");
        System.out.println("\n *** EXP1-1 *** \n");
        System.out.println("\n ************** \n");
        // computation for results
        for (int i = 0; i <numOfArr; i++) {
            int[] arr = clipArr(baseArr, arrLengths[i]);
            Random rand = new Random();
            int randNum;
            int maxTime = 0;
            int maxCount = 0;
            int maxAcc = 0;
            int timeElapsedAcc = 0;
            //START OF RUN - <numOfArr> times for each length of array
                for (int runs = 0; runs < numRuns; runs++) {
                    if (allRand == true) baseArr = makeRandArr(baseArr.length, 1000000);
                    RB_BST rb_bst = new RB_BST();
                    rb_bst.tree = buildRB_BST(arr);
                    randNum = rand.nextInt(1000000)-1000000/2;
                    //start timer
                    long startTime = System.nanoTime();

                    //COMPUTE
                    RB_BST.insert(rb_bst.tree, randNum);

                    //end timer
                    long endTime = System.nanoTime();
                    long timeElapsed = endTime - startTime;
                    timeElapsedAcc += timeElapsed; //time accumulator for averaging
                    if (timeElapsed > maxTime && timeElapsed < maxThreshold) maxTime = (int) timeElapsed;
                    maxCount++;
                    maxAcc += maxTime;

                    if ( (timeElapsedAcc / numRuns) > aveThreshold){ //clip for noisy results - redo run if value is too high
                        runs = 0;
                        maxTime = 0;
                        maxCount = 0;
                        maxAcc = 0;
                        timeElapsedAcc = 0;
                    }
                }//END OF RUN

                //save results for array of specified length and type - to .csv file
                csvWriter.write("RB-Insert");
                csvWriter.write(",");
                csvWriter.write(Integer.toString(arr.length));
                csvWriter.write(",");
                csvWriter.write(Integer.toString(timeElapsedAcc / numRuns));
                csvWriter.write(",");
                csvWriter.write(Integer.toString(maxTime));
                csvWriter.write(",");
                csvWriter.write(Integer.toString(maxAcc / maxCount));
                csvWriter.write("\n");

                //Output for user
                System.out.println("Array of length: " + arr.length);
                System.out.println("Over *" + numRuns + "* runs");
                System.out.println("Average Time elapsed: " + timeElapsedAcc / numRuns + " nanoseconds");
                System.out.println("Max Time elapsed: " + maxTime + " nanoseconds");
                System.out.println("Corrected Max Time elapsed: " + maxAcc / maxCount + " nanoseconds\n");


                if (i < 2) { //avoid printing massive trees
                    System.out.println("\n\nTrees for inpsection");
                    randNum = rand.nextInt(1000000)-1000000/2;
                    RB_BST rb_bst = new RB_BST();
                    rb_bst.tree = buildOS(arr);
                    RB_BST.insert(rb_bst.tree, randNum);
                    System.out.println("Tree for Array of length: " + arr.length);
                    PrintHelper.print2DUtilRB(rb_bst.tree, rb_bst.tree.root, 0);
                    System.out.println("\n");
                }

        }
        //save results - send to file and close
        csvWriter.flush();
    }
}



