/*
 * Copyright (C) 2019 chrisfourie.africa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Experiments;

import Tools.TreeComponents.Node;
import Tools.Algorithms.BST;
import Tools.Algorithms.OS;
import Tools.utilities.PrintHelper;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;

import static Tools.Algorithms.OS.buildOS;
import static Tools.utilities.ExperimentHelper.*;

/**
 *
 * @author chrisfourie.africa
 */
public class Exp2_2 {
    
    //EXPERIMENT 2_2 - OS-RANK
    public static void run (int[] baseArr, int[] arrLengths, FileWriter csvWriter, int numOfArr, int numRuns, boolean allRand, int maxThreshold, int aveThreshold) throws IOException {
        System.out.println("\n ************** \n");
        System.out.println("\n *** EXP2-2 *** \n");
        System.out.println("\n ************** \n");
        // computation for results
        for (int i = 0; i <numOfArr; i++) {
            int[] arr = clipArr(baseArr,arrLengths[i]);
            //START OF RUN - <numOfArr> times for each length of array
            int maxTime = 0;
            int maxCount = 0;
            int maxAcc = 0;
            int timeElapsedAcc = 0;
            for (int runs = 0; runs < numRuns; runs++) {
                if (allRand == true) baseArr = makeRandArr(baseArr.length, 1000000);
                OS os = new OS();
                os.rb_bst.tree = buildOS(arr);
                Node subjectNode = BST.searchIterative(os.rb_bst.tree.root, arr[(arr.length/2)]);
                //start timer
                long startTime = System.nanoTime();

                //COMPUTE
                int rank = OS.rank(os.rb_bst.tree, subjectNode);

                //end timer
                long endTime = System.nanoTime();
                long timeElapsed = endTime - startTime;
                timeElapsedAcc += timeElapsed; //time accumulator for averaging
                if (timeElapsed > maxTime && timeElapsed < maxThreshold) maxTime = (int)timeElapsed;  maxCount ++; maxAcc += maxTime;

                if ( (timeElapsedAcc / numRuns) > aveThreshold){ //clip for noisy results - redo run if value is too high
                    runs = 0;
                    maxTime = 0;
                    maxCount = 0;
                    maxAcc = 0;
                    timeElapsedAcc = 0;
                }
            }//END OF RUN

            //save results for array of specified length and type - to .csv file
            csvWriter.write("OS-Rank");
            csvWriter.write(",");
            csvWriter.write(Integer.toString(arr.length));
            csvWriter.write(",");
            csvWriter.write(Integer.toString(timeElapsedAcc/numRuns));
            csvWriter.write(",");
            csvWriter.write(Integer.toString(maxTime));
            csvWriter.write(",");
            csvWriter.write(Integer.toString(maxAcc/maxCount));
            csvWriter.write("\n");

            //Output for user
            System.out.println("Array of length: " + arr.length);
            System.out.println("Over *"+ numRuns + "* runs");
            System.out.println("Average Time elapsed: " + timeElapsedAcc/numRuns + " nanoseconds");
            System.out.println("Max Time elapsed: " + maxTime + " nanoseconds");
            System.out.println("Corrected Max Time elapsed: " + maxAcc/maxCount + " nanoseconds\n");

            if (i < 2) { //avoid printing massive trees
                System.out.println("\n\nTrees for inpsection");

                OS os = new OS();
                os.rb_bst.tree = buildOS(arr);

                System.out.println("Tree for Array of length: " + arr.length);
                Node subjectNode = BST.searchIterative(os.rb_bst.tree.root, arr[(arr.length / 2)]);
                int rank = OS.rank(os.rb_bst.tree, subjectNode);
                System.out.println("\nThe rank of " + subjectNode.key + " is " + rank);
                PrintHelper.print2DUtil(os.rb_bst.tree, os.rb_bst.tree.root, 0);
                System.out.println("\n");
            }

        }
        //save results - send to file and close
        csvWriter.flush();
    }
}
    


