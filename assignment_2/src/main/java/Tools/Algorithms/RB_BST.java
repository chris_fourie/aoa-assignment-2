package Tools.Algorithms;

import Tools.TreeComponents.*;
import Tools.utilities.PrintHelper;

/**
 *
 * @author chrisfourie.africa
 */
public class RB_BST {

    public Tree tree;

    public RB_BST() {
        this.tree = new Tree();
        tree.root = tree.nil;
    }

    //////////////////////////////
    // # Dynamic-set operations //
    /////////////////////////////
    public static void leftRotate(Tree tree, Node x) {
            Node y = x.right;       //set y 
            x.right = y.left;       // turn y's left subtree into x's right subtree 

            if (y.left != tree.nil) {
                y.left.p = x;
            }

            y.p = x.p;      // link x's parent to y 

            if (x.p == tree.nil) {
                tree.root = y;
            } else if (x == x.p.left) {
                x.p.left = y;
            } else {
                x.p.right = y;
            }
            y.left = x;     // put x on y's left 
            x.p = y;
    }

    public static void rightRotate(Tree tree, Node x) {
            Node y = x.left;         //set y 
            x.left = y.right;       // turn y's left subtree into x's right subtree 

            if (y.right != tree.nil) {
                y.right.p = x;
            }

            y.p = x.p;      // link x's parent to y 

            if (x.p == tree.nil) {
                tree.root = y;
            } else if (x == x.p.left) {
                x.p.left = y;
            } else {
                x.p.right = y;
            }
            y.right = x;     // put x on y's left 
            x.p = y;
    }

    public static void insert(Tree tree, int key) {
        Node z = new Node(key);
        Node y = tree.nil;
        Node x = tree.root;

        while (x != tree.nil) {
            y = x;
            if (z.key < x.key) {
                x = x.left;
            } else {
                x = x.right;
            }
        }

        z.p = y;

        if (y == tree.nil) {
            tree.root = z; // if tree is empty 
        } else if (z.key < y.key) {
            y.left = z;
        } else {
            y.right = z;
        }

        z.left = tree.nil;
        z.right = tree.nil;
        z.colour = 1; // 1 = RED

        insertFixup(tree, z);
    }

    public static void insertFixup(Tree tree, Node z) {
        while (z.p.colour == 1) { // 1 = RED  
            if (z.p == z.p.p.left) {
                Node y = z.p.p.right;
                if (y.colour == 1) {
                    z.p.colour = 0;
                    y.colour = 0;
                    z.p.p.colour = 1;
                    z = z.p.p;
                } else {
                    if (z == z.p.right) {
                        z = z.p;
                        leftRotate(tree, z);
                    }
                    z.p.colour = 0;
                    z.p.p.colour = 1; /// Typo in book
                    rightRotate(tree, z.p.p);
                }
            } else {
                Node y = z.p.p.left;
                if (y.colour == 1) {
                    z.p.colour = 0;
                    y.colour = 0;
                    z.p.p.colour = 1;
                    z = z.p.p;
                } else{
                    if (z == z.p.left) {
                        z = z.p;
                        rightRotate(tree, z);
                    }
                    z.p.colour = 0;
                    z.p.p.colour = 1; /// Typo in book
                    leftRotate(tree, z.p.p);
                }
            }
        }
        tree.root.colour = 0;
    }

    public static Tree buildRB_BST(int[] arr) {
        RB_BST rb_bst = new RB_BST();
        for (int i = 0; i < arr.length; i++) {
            RB_BST.insert(rb_bst.tree, arr[i]);
        }
        return rb_bst.tree;
    }

    public static void transplant(Tree tree, Node u, Node v) {
        if (u.p == tree.nil) {
            tree.root = v;
        } else if (u == u.p.left) {
            u.p.left = v;
        } else {
            u.p.right = v;
        }
        v.p = u.p;
    }

    public static void delete(Tree tree, Node z) {
        Node x;
        Node y = z;
        int yOriginalColour = y.colour;

        if (z.left == tree.nil) {
            x = z.right;
            transplant(tree, z, z.right);
        } else if (z.right == tree.nil) {
            x = z.left;
            transplant(tree, z, z.left);
        } else {
            y = BST.min(tree, z.right);
            yOriginalColour = y.colour;
            x = y.right;
            if (y.p == z) {
                x.p = y;
            } else {
                transplant(tree, y, y.right);
                y.right = z.right;
                y.right.p = y;
            }
            transplant(tree, z, y);
            y.left = z.left;
            y.left.p = y;
            y.colour = z.colour;
        }
        if (yOriginalColour == 0) {
            deleteFixup(tree, x);
        }
    }

    public static void deleteFixup(Tree tree, Node x) {
        Node w;

        while (x != tree.root && x.colour == 0) {
            if (x == x.p.left) {
                w = x.p.right;
                if (w.colour == 1) {
                    w.colour = 0;
                    x.p.colour = 1;
                    leftRotate(tree, x.p);
                    w = x.p.right;
                }
                if (w.left.colour == 0 && w.right.colour == 0) {
                    w.colour = 1;
                    x = x.p;
                } else {
                    if (w.right.colour == 0) {
                        w.left.colour = 0;
                        w.colour = 1;
                        rightRotate(tree, w);
                        w = x.p.right;
                    }
                    w.colour = x.p.colour;
                    x.p.colour = 0;
                    w.right.colour = 0;
                    leftRotate(tree, x.p);
                    x = tree.root;
                }
            } else {
                w = x.p.left;
                if (w.colour == 1) {
                    w.colour = 0;
                    x.p.colour = 1;
                    rightRotate(tree, x.p);
                    w = x.p.left;
                }
                if (w.right.colour == 0 && w.left.colour == 0) {
                    w.colour = 1;
                    x = x.p;
                } else {
                    if (w.left.colour == 0) {
                        w.right.colour = 0;
                        w.colour = 1;
                        leftRotate(tree, w);
                        w = x.p.left;
                    }
                    w.colour = x.p.colour;
                    x.p.colour = 0;
                    w.left.colour = 0;
                    rightRotate(tree, x.p);
                    x = tree.root;
                }
            }
        }
        x.colour = 0;
    }

    ///////////////
    //## queries //
    ///////////////
    //Use BST queries 
    ////////////////
    // # TESTING  //
    ///////////////
    public static void main(String[] args) {
        //basic test
        System.out.println("# Basic");
        RB_BST rb_bst = new RB_BST();
        RB_BST.insert(rb_bst.tree, 54);
        RB_BST.insert(rb_bst.tree, 9);
        RB_BST.insert(rb_bst.tree, 12);
        RB_BST.insert(rb_bst.tree, 14);
        RB_BST.insert(rb_bst.tree, 7);
        RB_BST.insert(rb_bst.tree, 3);
        RB_BST.insert(rb_bst.tree, 20);
        BST.treeWalk(rb_bst.tree, rb_bst.tree.root);

        //build test
        System.out.println("\n# Build");
        int[] arr = new int[]{16, 22, 4, 5, 8, 4, 22, -22, 22, 43, 21, 1000, -234, 4, 11, 0, 42, 2, 6, 81, 2, 19, 13, 12, 67, 84};
        rb_bst.tree = buildRB_BST(arr);
        BST.treeWalk(rb_bst.tree, rb_bst.tree.root);
        PrintHelper.print2DUtil(rb_bst.tree, rb_bst.tree.root, 0);

        //delete 
        System.out.println("\n # Delete");
        int keyInput = 22; //vary this to test 
        Node subjectNode = BST.searchIterative(rb_bst.tree.root, keyInput);
        delete(rb_bst.tree, subjectNode);
        subjectNode = BST.searchIterative(rb_bst.tree.root, keyInput);
        delete(rb_bst.tree, subjectNode);
        subjectNode = BST.searchIterative(rb_bst.tree.root, keyInput);
        delete(rb_bst.tree, subjectNode);
        PrintHelper.print2DUtil(rb_bst.tree, rb_bst.tree.root, 0);
        

    }

}
