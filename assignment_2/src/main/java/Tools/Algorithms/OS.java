/*
 * Copyright (C) 2019 chrisfourie.africa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Tools.Algorithms;

import Tools.TreeComponents.*;
import Tools.utilities.PrintHelper;

/**
 *
 * @author chrisfourie.africa
 */
public class OS {

    public RB_BST rb_bst;

    public OS() {
        this.rb_bst = new RB_BST();
    }

    public static Node select(Tree tree, Node x, int i) {

        int r = x.left.size + 1;
        if (i == r) {
            return x;
        } else if (i < r) {
            if (x.left.size == 0) return x; 
            return select(tree, x.left, i);
        } else {
            if (x.right.size == 0) return x; 
            return select(tree, x.right, i - r);
        }
        }
   

    public static int rank(Tree tree, Node x) {
        int r = x.left.size + 1;
        Node y = x;
        while (y != tree.root) {
            if (y == y.p.right) {
                r = r + y.p.left.size + 1;
            }
            y = y.p;
        }
        return r;
    }

    public static void leftRotate(Tree tree, Node x) {
            Node y = x.right;       //set y 
            x.right = y.left;       // turn y's left subtree into x's right subtree 

            if (y.left != tree.nil) {
                y.left.p = x;
            }

            y.p = x.p;      // link x's parent to y 

            if (x.p == tree.nil) {
                tree.root = y;
            } else if (x == x.p.left) {
                x.p.left = y;
            } else {
                x.p.right = y;
            }
            y.left = x;     // put x on y's left 
            x.p = y;

            y.size = x.size;
            x.size = x.left.size + x.right.size + 1;
    }

    public static void rightRotate(Tree tree, Node x) {
            Node y = x.left;         //set y 
            x.left = y.right;       // turn y's left subtree into x's right subtree 

            if (y.right != tree.nil) {
                y.right.p = x;
            }

            y.p = x.p;      // link x's parent to y 

            if (x.p == tree.nil) {
                tree.root = y;
            } else if (x == x.p.left) {
                x.p.left = y;
            } else {
                x.p.right = y;
            }
            y.right = x;     // put x on y's left 
            x.p = y;

            y.size = x.size;
            x.size = x.left.size + x.right.size + 1;
    }

    public static void insert(Tree tree, int key) {
        Node z = new Node(key);
        Node y = tree.nil;
        Node x = tree.root;

        //Phase 1 - downward traversal 
        //increment each sub-tree's root along simple path 
        while (x != tree.nil) {
            x.size++; //OS
            y = x;
            if (z.key < x.key) {
                x = x.left;
            } else {
                x = x.right;
            }
        }

        z.p = y;

        if (y == tree.nil) {
            tree.root = z; // if tree is empty 
        } else if (z.key < y.key) {
            y.left = z;
        } else {
            y.right = z;
        }

        z.left = tree.nil;
        z.right = tree.nil;
        z.colour = 1; // 1 = RED
        z.size = 1; 

        //Phase 2 - upward traversal 
        insertFixup(tree, z);
    }

    public static void insertFixup(Tree tree, Node z) {
        while (z.p.colour == 1) { // 1 = RED  
            if (z.p == z.p.p.left) {
                Node y = z.p.p.right;
                if (y.colour == 1) {
                    z.p.colour = 0;
                    y.colour = 0;
                    z.p.p.colour = 1;
                    z = z.p.p;
                } else {
                    if (z == z.p.right) {
                        z = z.p;
                        leftRotate(tree, z);
                    }
                    z.p.colour = 0;
                    z.p.p.colour = 1; /// Typo in book
                    rightRotate(tree, z.p.p);
                }
            } else {
                Node y = z.p.p.left;
                if (y.colour == 1) {
                    z.p.colour = 0;
                    y.colour = 0;
                    z.p.p.colour = 1;
                    z = z.p.p;
                } else{
                    if (z == z.p.left) {
                        z = z.p;
                        rightRotate(tree, z);
                    }
                    z.p.colour = 0;
                    z.p.p.colour = 1; /// Typo in book
                    leftRotate(tree, z.p.p);
                }
            }
        }
        tree.root.colour = 0;
    }

    public static Tree buildOS(int[] arr) {
        OS os = new OS();
        for (int i = 0; i < arr.length; i++) {
            OS.insert(os.rb_bst.tree, arr[i]);
        }
        return os.rb_bst.tree;
    }

    public static void transplant(Tree tree, Node u, Node v) {
        if (u.p == tree.nil) {
            tree.root = v;
        } else if (u == u.p.left) {
            u.p.left = v;
        } else {
            u.p.right = v;
        }
        v.p = u.p;
    }

    public static void delete(Tree tree, Node z) {
        Node x;
        Node y = z;
        int yOriginalColour = y.colour;
        
        // decrement all ancestors of z
        Node d = z; 
        while (d.p != tree.nil){
            d.p.size --;
            d = d.p; 
        }
        
        //Phase 1  
        if (z.left == tree.nil) {
            x = z.right;
            transplant(tree, z, z.right);
        } else if (z.right == tree.nil) {
            x = z.left;
            transplant(tree, z, z.left);
        } else {
            y = BST.min(tree, z.right);
            yOriginalColour = y.colour;
            x = y.right;
            if (y.p == z) {
                x.p = y;
            } else {
                transplant(tree, y, y.right);
                y.right = z.right;
                y.right.p = y;
            }
            transplant(tree, z, y);
            y.left = z.left;
            y.left.p = y;
            y.colour = z.colour;
        }
        if (yOriginalColour == 0) {
            //Phase 2 
            deleteFixup(tree, x);
        }
        
        
    }

    public static void deleteFixup(Tree tree, Node x) {
        Node w;

        while (x != tree.root && x.colour == 0) {
            if (x == x.p.left) {
                w = x.p.right;
                if (w.colour == 1) {
                    w.colour = 0;
                    x.p.colour = 1;
                    leftRotate(tree, x.p);
                    w = x.p.right;
                }
                if (w.left.colour == 0 && w.right.colour == 0) {
                    w.colour = 1;
                    x = x.p;
                } else {
                    if (w.right.colour == 0) {
                        w.left.colour = 0;
                        w.colour = 1;
                        rightRotate(tree, w);
                        w = x.p.right;
                    }
                    w.colour = x.p.colour;
                    x.p.colour = 0;
                    w.right.colour = 0;
                    leftRotate(tree, x.p);
                    x = tree.root;
                }
            } else {
                w = x.p.left;
                if (w.colour == 1) {
                    w.colour = 0;
                    x.p.colour = 1;
                    rightRotate(tree, x.p);
                    w = x.p.left;
                }
                if (w.right.colour == 0 && w.left.colour == 0) {
                    w.colour = 1;
                    x = x.p;
                } else {
                    if (w.left.colour == 0) {
                        w.right.colour = 0;
                        w.colour = 1;
                        leftRotate(tree, w);
                        w = x.p.left;
                    }
                    w.colour = x.p.colour;
                    x.p.colour = 0;
                    w.left.colour = 0;
                    rightRotate(tree, x.p);
                    x = tree.root;
                }
            }
        }
        x.colour = 0;
    }

    ///////////////
    //## queries //
    ///////////////
    //Use BST queries 
    ////////////////
    // # TESTING  //
    ///////////////
    public static void main(String[] args) { 
        //basic test
        System.out.println("# Basic");
        OS os = new OS();
        OS.insert(os.rb_bst.tree, 54);
        OS.insert(os.rb_bst.tree, 9);
        OS.insert(os.rb_bst.tree, 12);
        OS.insert(os.rb_bst.tree, 14);
        OS.insert(os.rb_bst.tree, 7);
        OS.insert(os.rb_bst.tree, 3);
        OS.insert(os.rb_bst.tree, 20);
        BST.treeWalk(os.rb_bst.tree, os.rb_bst.tree.root);
        PrintHelper.print2DUtil(os.rb_bst.tree, os.rb_bst.tree.root, 0);

        //build test
        System.out.println("\n# Build");
        int[] arr = new int[]{16, 22, 4, 5, 8, 4, 22, 22, 22, 43, 21, 1000, 234, 19, 11, 1, 42, 2, 6, 81, 2, 19, 13, 12, 67, 84};
        os.rb_bst.tree = buildOS(arr);
        BST.treeWalk(os.rb_bst.tree, os.rb_bst.tree.root);
        PrintHelper.print2DUtil(os.rb_bst.tree, os.rb_bst.tree.root, 0);
        
        //delete 
        System.out.println("\n \n # Delete");
        int keyInput = arr[arr.length/2]; //vary this to test 
       
        Node subjectNode = BST.searchIterative(os.rb_bst.tree.root, keyInput);
        delete(os.rb_bst.tree, subjectNode);
        PrintHelper.print2DUtil(os.rb_bst.tree, os.rb_bst.tree.root, 0);
        
//        subjectNode = BST.searchIterative(os.rb_bst.tree.root, keyInput);
//        delete(os.rb_bst.tree, subjectNode);
//        PrintHelper.print2DUtil(os.rb_bst.tree, os.rb_bst.tree.root, 0);
//        
//        subjectNode = BST.searchIterative(os.rb_bst.tree.root, keyInput);
//        delete(os.rb_bst.tree, subjectNode);
//        PrintHelper.print2DUtil(os.rb_bst.tree, os.rb_bst.tree.root, 0);

        //select 
        System.out.println();
        Node s = OS.select(os.rb_bst.tree, os.rb_bst.tree.root, (13));
        System.out.println("\n \n Selected: " + s.key + " from rank: " + (arr.length/2));
        
        //rank 
        subjectNode = BST.searchIterative(os.rb_bst.tree.root, keyInput);
        int rank = OS.rank(os.rb_bst.tree, subjectNode);
        System.out.println("\n \n The rank of " + subjectNode.key + " is " + rank);
        

    }

}
