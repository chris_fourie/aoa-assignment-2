package Tools.Algorithms;
// Reference: https://www.geeksforgeeks.org/binary-search-tree-set-1-search-and-insertion/
import Tools.TreeComponents.*;
import Tools.utilities.PrintHelper;


public class BST {
     Tree tree;

    public BST() {
        this.tree = new Tree();
    }
    
    //Build From array to tree
    //Uses Insert(key) dynamic set function
    public static Tree buildBST(int[] arr) {
        Tree tree = new Tree();
        for (int i = 0; i < arr.length; i++) {
            BST.insertIt(tree,arr[i]);
        }
        return tree ;
    }
    
    //////////////////////////////
    // # Dynamic-set operations //
    /////////////////////////////

    //Insert(t.root, z) //insert Iterative 
    public static void insertIt (Tree tree, int key){
    Node z = new Node(key); 
    Node y = null; 
    Node x = tree.root; //null on first pass of empty tree  
    
    while (x != null){ 
        y = x; 
        if (z.key < x.key) x = x.left;
        else x = x.right; 
    }
    
    z.p = y; 
    
    if (y == null) tree.root = z;  // if tree is emprty 
    else if ( z.key < y.key) y.left = z; 
    else y.right = z; 
    }
    
    // Traverse the tree
    public static void treeWalk(Tree tree, Node x){
        if (x == tree.nil){} // do nothing 
        else if (x != null) {
            treeWalk(tree, x.left);
            System.out.println(x.key);
            treeWalk(tree, x.right);
        }
    }

    ///////////////
    //## queries //
    ///////////////
    
    //Search(T.root,key)
    public static Node searchIterative (Node x, int key){
        while (x != null && key != x.key){
            if (key < x.key) x = x.left; 
            else x = x.right; 
        }
        return x;   /*if key is not present, will return null
                    other wise will return node containg key*/
    }
    
    //Min(T.root)
    public static Node min(Tree tree, Node x){ 
        while (x.left != null && x.left != tree.nil){
            x = x.left; 
        }
        return x; 
    }
    
    //Max(T.root)
    public static Node max(Tree tree, Node x){ 
        while (x.right != null && x.right != tree.nil){
            x = x.right; 
        }
        return x; 
    }
    
    //Pred(x)
    public static Node predecessor (Tree tree, Node x){ 
        //Node is 'below' current node -> traverse down
        if (x.left != null)
        {
            return BST.max(tree, x.left);
        }
        
        //Node is 'above' current node -> traverse up 
        Node y = x.p;  
        while (y != null && x == y.left){
            x = y; 
            y = y.p; 
        }
        return y; 
    }
    
    //Succ(x)
    public static Node successor(Tree tree, Node x){ 
        //Node is 'below' current node -> traverse down
        if (x.right != null)
        {
            return BST.min(tree, x.right);
        }
        
        //Node is 'above' current node -> traverse up 
        Node y = x.p;  
        while (y != null && x == y.right){
            x = y; 
            y = y.p; 
        }
        return y; 
    }
    
    ///////////////
    //## Delete //
    //////////////
    
    public static void transplant(Tree tree, Node u, Node v){
        //detatch u and insert v
        if (u.p == null) tree.root = v; //root node 
        else if (u == u.p.left) u.p.left = v; //left node 
        else u.p.right = v ; //right node    
        if (v != null) v.p = u.p; 
    }
    
    public static void delete(Tree tree, Node z){
        if (z.left == null) BST.transplant(tree,z,z.right);
        else if (z.right == null) BST.transplant(tree, z, z.left);
        else {
            Node y = BST.min(tree, z.right);
            if (y.p != z){
                BST.transplant(tree,y,y.right);
                y.right = z.right; 
                z.right.p = y; 
                }
            BST.transplant(tree,z,y);
            y.left = z.left; 
            y.left.p = y; 
        }
        
        
    }
    
    
    ////////////////
    // # TESTING //
    ///////////////
    
    public static void main(String[] args) {
        //basic test
        System.out.println("# Basic");
        BST bst = new BST(); 
        BST.insertIt(bst.tree, 53);
        BST.insertIt(bst.tree, 12);
        BST.insertIt(bst.tree, 5);
        BST.insertIt(bst.tree, 65);
        BST.treeWalk(bst.tree, bst.tree.root);

        //build test
        System.out.println("\n# Build");
        int[] arr = new int[] {16,22,123,3,19,4,7,8,4,5,8,4,2,6,81,2}; //TODO does not handle duplicate data - bug / feature??
        bst.tree = buildBST(arr);
        BST.treeWalk(bst.tree, bst.tree.root);
            

        //search
        System.out.println("\n# Search");
        System.out.println(BST.searchIterative(bst.tree.root,8));
        System.out.println(BST.searchIterative(bst.tree.root,24));
        
        //min/max 
        System.out.println("\n# Min / Max");
        System.out.println("Min: "+ BST.min(bst.tree, bst.tree.root).key);
        System.out.println("Max: "+ BST.max(bst.tree, bst.tree.root).key);
        
        //Succ / Pred
        System.out.println("\n# Successor & Predecessor");
        int keyInput = 22; //vary this to test 
        Node subjectNode = BST.searchIterative(bst.tree.root, keyInput);
        Node pred = BST.predecessor(bst.tree, subjectNode);
        Node succ = BST.successor(bst.tree, subjectNode); 
        System.out.println("The predecessor of " + subjectNode.key + " is: " + pred.key + "\nand the successor is: " + succ.key);
        
        // inspect tree      
        PrintHelper.print2DUtil(bst.tree, bst.tree.root, 0);
        
        //delete 
        BST.delete(bst.tree, subjectNode);
        
        // inspect tree 
        System.out.println("\n");
        PrintHelper.print2DUtil(bst.tree, bst.tree.root, 0);
    
    }
}
