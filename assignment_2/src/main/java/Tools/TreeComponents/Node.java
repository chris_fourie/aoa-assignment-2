package Tools.TreeComponents;

public class Node {
    public int key, colour, size; 
    public Node p, left, right;
    
    // variables needed to print the tree like a tree
    public int depth, level, drawPos;

    //the only input should be the key, as this determines the placement of the node in the tree via the insert function
    public Node (int key) {
        this.key = key; 
        //this.colour = 0; not required as every new node is initially red 
        this.p = this.left = this.right = null;
        
//        // variables needed to print the tree like a tree
//        this.depth = this.level = this.drawPos = 0;
    }

    //TESTING//
    public static void main(String[] args) {
        Node root = new Node(1);
        System.out.println(root.key);
    }
}
