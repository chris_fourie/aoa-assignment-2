package Tools.utilities;
import Tools.TreeComponents.*;

/**
 *
 * @author fofo
 */
public class PrintHelper {
    
    public static void print2DUtil(Tree tree, Node root, int space){
        int COUNT = 1;

        // Base case
        if (root == null || root == tree.nil)
            return;

        // Increase distance between levels
        space += COUNT;

        // Process right child first
        print2DUtil(tree, root.right, space);

        // Print current node after space
        // count
        System.out.print("\n");
        for (int i = COUNT; i < space; i++)
            System.out.print(" - ");  //each '-' indicates an h level
        System.out.print("|"+root.key + ":" +root.colour + ":" + root.size);  // '-'*height + key:colour:size

        // Process left child
        print2DUtil(tree, root.left, space);
    }

    public static void print2DUtilRB(Tree tree, Node root, int space){
        int COUNT = 1;

        // Base case
        if (root == null || root == tree.nil)
            return;

        // Increase distance between levels
        space += COUNT;

        // Process right child first
        print2DUtilRB(tree, root.right, space);

        // Print current node after space
        // count
        System.out.print("\n");
        for (int i = COUNT; i < space; i++)
            System.out.print(" - ");  //each '-' indicates an h level
        System.out.print("|"+root.key + ":" +root.colour);  // '-'*height + key:colour:size

        // Process left child
        print2DUtilRB(tree, root.left, space);
    }
}
