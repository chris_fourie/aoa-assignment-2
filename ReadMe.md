# Applicaiton of Algorithms - Assignment 2 2019 - Chris Fourie - 35183

## To Run 

* Navigate to and run `../src/main/java/Experiments/Run_Experiments.java`
* This will output the results of the 4 experiments into a results folder here  `../results`

### Outputs 

Outputs for each size array are displayed with smaller arrays being displayed as simplified trees that can be inspected for correctness, see report for interpretation guide. Each experiment also outputs relevant information to demonstrate that the designated operation is being carried out. 

### Hyperparameters

  There are various hyperparameters that can be set. To vary them edit `../src/main/java/Experiments/Run_Experiments.java`

  The following may be of interest: 

  * allRand -> True will make each run use a random array 
  * Thresholds -> these determine were to clip and redo runs to mitigate noisy results 

## Algorithms

* Navigate to `../src/main/java/Tools/Algorithms`

* Here BST, RB_BST and OS algorithm implementations can be found for inspection 

* RB is extends BST via composition and OS extends RB_BST by composition as well 

  

## Tree Components

* Generic Tree Components i.e. a node and tree class are used for each algorithm 

## Utilities 

* This contains classes used for displaying the trees and making random arrays 





 
